package co.simplon;

public class Grid {
 ///Valeurs constantes : taille de la gride et nombre de serpents(snakes) et d'échelles(ladders)
    private final int ROWS = 10;
	private final int COLS = 10;
    private final int NumSnakes = 5;
	private final int NumLadders = 6;
 //Variables du la gride
    private int[][] gameGrid;
	private int[][] snakes;
	private int[][] ladders;

    private Players players;



// ROWS X COLS 
public void grids() {
    
 gameGrid= new int[ROWS][COLS];
for (int row = 0; row < ROWS; row++){
    for (int col = 0; col < COLS; col++){
        gameGrid[row][col] = row*ROWS + col + 1;
        System.out.print(" ");
        System.out.print(gameGrid[row][col]);
    }
System.out.println(" ");
}
}

//position de serpent
public void setSnakes(){
    snakes = new int[NumSnakes][2];

    snakes[0][0] = 23;
    snakes[0][1] = 4;
    snakes[1][0] = 46;
    snakes[1][1] = 33;
    snakes[2][0] = 62;
    snakes[2][1] = 41;
    snakes[3][0] = 85;
    snakes[3][1] = 65;
    snakes[4][0] = 92;
    snakes[4][1] = 69;

  
    
}
//position des échelles
public void setLadders(){
    ladders = new int[NumLadders][2];

    ladders[0][0] = 12;
    ladders[0][1] = 30;
    ladders[1][0] = 15;
    ladders[1][1] = 44;
    ladders[2][0] = 20;
    ladders[2][1] = 22;
    ladders[3][0] = 54;
    ladders[3][1] = 68;
    ladders[4][0] = 78;
    ladders[4][1] = 82;
    ladders[5][0] = 87;
    ladders[5][1] = 93;
    
}

public void positions() {

    players = new Players();
    players.deplacement();
    int position1 = players.depjoueur1;
    int position2 = players.depjoueur2;
   
   for (int idx = 0; idx < NumSnakes; idx++){
    if (snakes[idx][0] == position1){
        
        position1 = snakes[idx][1];
        
        System.out.println( " prend le serpent de " + snakes[idx][0] + " a " + snakes[idx][1]);

     }   
    }   

    for (int idx = 0; idx < NumSnakes; idx++){
        if (snakes[idx][0] == position2){
            
            position2 = snakes[idx][1];
            
            System.out.println( " prend le serpent de " + snakes[idx][0] + " a " + snakes[idx][1]);
    
         }   
        }   

        for (int idx = 0; idx < NumLadders; idx++){
            if (ladders[idx][0] == position1){
                
                position1 = ladders[idx][1];
                
                System.out.println( " prend l'échelle de " + ladders[idx][0] + " a " + ladders[idx][1]);
        
             }   
            }   

            for (int idx = 0; idx < NumLadders; idx++){
                if (ladders[idx][0] == position2){
                    
                    position2 = ladders[idx][1];
                    
                    System.out.println( " prend l'échelle de " + ladders[idx][0] + " a " + ladders[idx][1]);
            
                 }   
                }   
}
    
}
